﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DualSpot : MonoBehaviour {
	public GameObject dual;
	public GameObject dualMap;
	public GameManager gm;
	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		
			if (gm.turn == 1) {
				gm.p1Duals--;
			} else if (gm.turn == 2) {
				gm.p2Duals--;

			}
			gm.DualUsed ();
			GameObject newDual = Instantiate (dual);
			newDual.transform.position = transform.position;
			dualMap.SetActive (false);
	}
}
