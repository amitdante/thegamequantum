﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveData : MonoBehaviour {
	public int totalAchievements;
	public int contiWins;
	public int contiLoss;
	public int difficultyLevel;
	public int wins;
	public int losses;
	public int lastResult;
	public int score;
	public List<int> uniqueReactionsUnlocked;
	void Start(){
		
		Load ();
		if(difficultyLevel==0){
			difficultyLevel = 2;
		}
		if(contiWins>=3&&difficultyLevel<=5){
			difficultyLevel++;
			contiWins = 0;
		}
		if(contiLoss>=3&&difficultyLevel>2){
			difficultyLevel--;
			contiLoss = 0;
		}
		if(uniqueReactionsUnlocked==null){
			uniqueReactionsUnlocked = new List<int> ();
		}

	}

	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/savedgame3.dat");
		Data data = new Data ();
		data.totalAchievements = totalAchievements;
		data.contiLoss = contiLoss;
		data.contiWins = contiWins;
		data.difficultyLevel = difficultyLevel;
		data.wins = wins;
		data.losses = losses;
		data.lastResult = lastResult;
		data.uniqueReactionsUnlocked = uniqueReactionsUnlocked;
		data.score = score;
		bf.Serialize (file, data);
		file.Close ();
	}

	public void Load(){
		if(File.Exists(Application.persistentDataPath + "/savedgame3.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedgame3.dat",FileMode.Open);
			Data data = (Data)bf.Deserialize(file);
			totalAchievements = data.totalAchievements;
			contiLoss = data.contiLoss;
			contiWins = data.contiWins;
			difficultyLevel = data.difficultyLevel;
			wins = data.wins;
			losses = data.losses;
			lastResult = data.lastResult;
			score = data.score;
			uniqueReactionsUnlocked = data.uniqueReactionsUnlocked;
			file.Close ();
		}
	}

}

[System.Serializable]
class Data {
	public int totalAchievements;
	public int contiWins;
	public int contiLoss;
	public int difficultyLevel;
	public int wins;
	public int losses;
	public int lastResult;
	public int score;
	public List<int> uniqueReactionsUnlocked;


}
