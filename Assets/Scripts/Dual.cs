﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dual : MonoBehaviour {
	public LayerMask layer;
	Collider2D[] foundCollider;
	GameManager gm;
	public List<GameObject> electrons;
	List<GameObject> nucleuses;

	// Use this for initialization
	void Start () {
		electrons = new List<GameObject> ();
		nucleuses = new List<GameObject> ();
		gm = FindObjectOfType<GameManager> ();
		foundCollider =  Physics2D.OverlapCircleAll (transform.position, 1 , layer);
		FindNucleus ();
	}
	
	// Update is called once per frame
	void Update () {
		if(electrons.Count == 0 ){
			foreach(GameObject nucleus in nucleuses){
				nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
				Destroy (nucleus);
			}
			Destroy (gameObject);
		}
	}

	void FindNucleus(){

		if(gm.turn == 1){
			foreach(Collider2D col in foundCollider){
				if(col.gameObject.tag == "Nucleus2"){
					nucleuses.Add (col.gameObject);
					foreach (GameObject electron in col.gameObject.GetComponent<ElectronSpawner> ().electrons) {
						electrons.Add (electron);
					}
				}
			}
			MoveElectrons ();
		} else if(gm.turn == 2){
			foreach(Collider2D col in foundCollider){
				
				if(col.gameObject.tag == "Nucleus1"){
					nucleuses.Add (col.gameObject);
					foreach (GameObject electron in col.gameObject.GetComponent<ElectronSpawner> ().electrons) {
						electrons.Add (electron);
					}
				}
			}
			MoveElectrons ();
		}

	}

	void MoveElectrons(){
		foreach(GameObject electron in electrons){
			electron.GetComponent<Rotator> ().MoveToDual (transform.position);
		}
	}
	void OnTriggerEnter2D(Collider2D other){

		if(other.gameObject.tag == "electron1" || other.gameObject.tag == "electron2"){
			GameObject nucleus = other.gameObject.transform.parent.gameObject;
			nucleus.GetComponent<ElectronSpawner> ().electrons.Remove (other.gameObject);
			nucleus.transform.GetChild (0).gameObject.SetActive(false);
			nucleus.transform.GetChild (1).gameObject.SetActive(false);
			nucleus.transform.GetChild (2).gameObject.SetActive(false);
			nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpriteRenderer>().enabled = true;
			//nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
			Destroy (nucleus.GetComponent<ElectronSpawner> ().vNucleus);
			electrons.Remove (other.gameObject);
			Destroy (other.gameObject);
		}
	}
}
