﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpawnPointsScript : MonoBehaviour,IPointerClickHandler {
	public int type;
	public int side;
	public Text pointText;
	public BoardAI ai;
	public int x;
	public int y;
	public Move m;
	public bool spawned;


	[SerializeField]
	private GameObject nucleus1;
	[SerializeField]
	private GameObject nucleus2;

	private GameManager gm;

	// Use this for initialization
	void Start () {
		spawned = false;
		m = new Move (this, 0, GetInstanceID(), x, y, 0, 0, false);
		if(type==1){
			m.criticalMass = 2;
		}else if(type==2){
			m.criticalMass = 3;
		}else if(type==3){
			m.criticalMass = 4;
		}

		ai.board.allMoves.Add (m);
		gm = FindObjectOfType<GameManager> ();
		//Debug.Log (gameObject.name + " : " + GetInstanceID ());

	}
	
	// Update is called once per frame
	void Update () {
	}

	#region IPointerClickHandler implementation


	public void OnPointerClick (PointerEventData eventData)
	{
		if (gm.turn ==1 && !spawned) {
			spawned = true;
			NucleusSpawner ();
			gm.ChangeTurn (2);
			gm.AIPlay ();
		}
	}


	#endregion

		
	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.tag == "electron1" && !spawned){
			gm.player1Score--;
			spawned = true;
			Destroy (other.gameObject);
			NucleusSpawner ();
			
		}else if(other.gameObject.tag == "electron2" && !spawned){
			spawned = true;
			Destroy (other.gameObject);
			NucleusSpawnerAI();
		}
	}

	public void NucleusSpawnerAI(){
		var newNucleus = Instantiate (nucleus2) as GameObject;
		m.owner = 2;
		GetComponent<SpriteRenderer> ().enabled = false;
		transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = true;
		newNucleus.transform.SetParent (gameObject.transform);
		newNucleus.GetComponent<ElectronSpawner> ().type = type;
		newNucleus.GetComponent<ElectronSpawner> ().side = side;
		newNucleus.GetComponent<ElectronSpawner> ().spawnPoint = gameObject;
		newNucleus.transform.position = this.transform.position;
		GetComponent<CircleCollider2D> ().enabled = false;
		//GetComponent<SpriteRenderer> ().enabled = false;

	}

	void NucleusSpawner(){
		var newNucleus = Instantiate (nucleus1) as GameObject;
		m.owner = 1;
		GetComponent<SpriteRenderer> ().enabled = true;
		transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = false;
		newNucleus.GetComponent<ElectronSpawner> ().type = type;
		newNucleus.GetComponent<ElectronSpawner> ().side = side;
		newNucleus.GetComponent<ElectronSpawner> ().spawnPoint = gameObject;
		newNucleus.transform.position = this.transform.position;
		GetComponent<CircleCollider2D> ().enabled = false;
		//GetComponent<SpriteRenderer> ().enabled = false;

	}


	public void SetNucleusPoint(){
		Debug.Log ("I am Called");
		pointText.transform.position = transform.position;
		pointText.text = "X" + gm.crNos.ToString ();
		Invoke ("RemoveNucleusPoint", 0.7f);
	}

	public void RemoveNucleusPoint(){
		pointText.text = "";
	}
}
