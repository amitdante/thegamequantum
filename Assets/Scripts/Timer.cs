﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
	public float timer;
	public float timer2;

	void Start () {
		
	}
	

	void Update () {
		
	}

	public void ResetTimer(){
		CancelInvoke ("DecreaseTimer");
		timer = 0.6f;
		InvokeRepeating ("DecreaseTimer", 0.1f, 0.10f);
	}
	void DecreaseTimer(){
		timer -= 0.1f;
		if (timer <= 0){
			CancelInvoke ("DecreaseTimer");
			GetComponent<GameManager> ().lastReactionPoint.GetComponent<SpawnPointsScript> ().SetNucleusPoint ();
			GetComponent<GameManager> ().UpdateAchievement ();
			GetComponent<GameManager>().ChangeTurn (2);
			GetComponent<GameManager>().AIPlay ();



		}
	}
	public void ResetTimer2(){
		CancelInvoke ("DecreaseTimer2");
		timer2 = 1;
		InvokeRepeating ("DecreaseTimer2", 0.1f, 0.10f);
	}
	void DecreaseTimer2(){
		timer2 -= 0.1f;
		if (timer2 <= 0){
			CancelInvoke ("DecreaseTimer2");

			GetComponent<GameManager>().ChangeTurn (1);
		}
	}
}
