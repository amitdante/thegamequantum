﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchBase : MonoBehaviour {

	public RectTransform canvas;
	RectTransform button;
	public RectTransform limit;
	Vector3 startingPosition;
	public float speed;
	bool canMoveUp;
	bool canMoveDown;

	void Start()
	{
		button = gameObject.GetComponent<RectTransform>();
		//canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
		startingPosition = transform.position;
		speed = 2f;
		canMoveUp = true;
		canMoveDown = false;

	}

	void Update () {
		if (canMoveUp) {
			transform.Translate (speed*Time.deltaTime,0f, 0f);
			if (button.position.x >= limit.position.x) {
				canMoveUp = false;
			}
		} else if (canMoveDown) {
			transform.Translate (-speed*Time.deltaTime,0f, 0f);
			if (button.position.x <= startingPosition.x) {
				canMoveDown = false;
			}
		}
	}
	public void MoveUp(){
		canMoveDown = false;
		canMoveUp = true;
	}
	public void MoveDown(){
		canMoveDown = true;
		canMoveUp = false;
	}
}
