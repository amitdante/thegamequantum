﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PanelSlider : MonoBehaviour {
	public Button yes;
	public Button no;
	public Text quitText;
	public Button quit;
	public Button menu;
	Vector2 startingPosition;
	public float speed;
	public GameObject topLimit;
	public Button musicOn;
	public Text musicText;
	public Button soundOn;
	public Text soundText;
	public Button tutorial;
	public Image madamCurie;
	public GameObject winScreen;
	public GameObject looseScreen;
	public Text resultNumbersWin;
	public Text resultNumbersLoose;
	public SaveData sd;
	public Text winScore;
	public Text looseScore;
	public Text winAch;
	public Text looseAch;
	public GameObject scoreBase;
	public GameObject achBase;

	int direction;
	bool canMove;
	Vector3 screenVector;
	private bool up;
	private GameManager gm;
	void Start()
	{	
		direction = 0;
		canMove = false;
		startingPosition = transform.position;
		speed = 50f;
		up = false;
		screenVector = Camera.main.ScreenToWorldPoint (new Vector3(Screen.width, Screen.height,1));
		screenVector.x = transform.position.x;
		gm = FindObjectOfType<GameManager> ();
	}

	void Update () {
//		if (canMove && direction==1) {
//			transform.Translate (0f, speed * Time.deltaTime, 0f);
//			if (transform.position.y >= screenVector.y) {
//				canMove = false;
//
//				direction = 0;
//				up = true;
//			}
//		}
		if (canMove && direction==-1) {
			transform.Translate (0f, -speed*Time.deltaTime, 0f);
			StartCoroutine (ShowOptions (2));
			//float screenY = Camera.main.ViewportToWorldPoint ();
			if (transform.position.y <= startingPosition.y/2) {
				canMove = false;
				transform.position = startingPosition;
				direction = 0;
				up = false;
				madamCurie.gameObject.SetActive (true);

			}
		}
	}

	public void MoveUPforMenu(){
		if (!up) {
			iTween.MoveTo(gameObject,iTween.Hash("position",screenVector,"time",0.8f,"easetype",iTween.EaseType.easeOutBounce,"oncomplete","MoveUpComplete"));
			direction = 1;
			madamCurie.gameObject.SetActive (false);
			StartCoroutine (ShowOptions (3));

		}else{
			canMove = true;
			direction= - 1;
		}

	}

	public void MoveUP(){
		if (!up) {
			//canMove = true;
			iTween.MoveTo(gameObject,iTween.Hash("position",screenVector,"easetype",iTween.EaseType.easeOutBounce,"time",0.8f,"oncomplete","MoveUpComplete"));
			direction = 1;
			madamCurie.gameObject.SetActive (false);
			StartCoroutine (ShowOptions (1));
		}else{
			MoveDown ();
		}

	}

	public void MoveDown(){
		canMove = true;
		direction = -1;


	}


	void MoveUpComplete(){
		iTween.Stop ();
		up = true;
	}

	public void WinScreen (){
		scoreBase.SetActive (false);
		achBase.SetActive (false);
		resultNumbersWin.text = sd.wins+"/"+sd.losses;
		winScore.text = gm.player1Score.ToString ();
		winAch.text = sd.totalAchievements + "/28";
		winScreen.SetActive (true);
		iTween.MoveTo(gameObject,iTween.Hash("position",screenVector,"easetype",iTween.EaseType.easeOutBounce,"time",0.8f,"oncomplete","MoveUpComplete"));
		StartCoroutine (ShowOptions (4));
	}
	public void LooseScreen (){
		scoreBase.SetActive (false);
		achBase.SetActive (false);
		looseScore.text = gm.player1Score.ToString ();
		looseAch.text = sd.totalAchievements + "/28";
		resultNumbersLoose.text = sd.wins+"/"+sd.losses;
		looseScreen.SetActive (true);
		iTween.MoveTo(gameObject,iTween.Hash("position",screenVector,"easetype",iTween.EaseType.easeOutBounce,"time",0.8f,"oncomplete","MoveUpComplete"));
		StartCoroutine (ShowOptions (4));
	}

	public void PlayAgain(){
		int scene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (scene, LoadSceneMode.Single);
	}

	IEnumerator ShowOptions(int n){

		if (n == 1) {
			yield return new WaitForSeconds (0.5f);
			yes.gameObject.SetActive (true);
			no.gameObject.SetActive (true);
			quitText.gameObject.SetActive (true);
			quit.gameObject.SetActive (false);
			menu.gameObject.SetActive (false);

		}else if(n==2){
			yes.gameObject.SetActive (false);
			no.gameObject.SetActive (false);
			quitText.gameObject.SetActive (false);
			quit.gameObject.SetActive (true);
			menu.gameObject.SetActive (true);
			soundOn.gameObject.SetActive (false);
			soundText.gameObject.SetActive (false);
			musicOn.gameObject.SetActive (false);
			musicText.gameObject.SetActive (false);
			tutorial.gameObject.SetActive (false);
		}else if(n==3){
			yield return new WaitForSeconds (0.5f);
			soundOn.gameObject.SetActive (true);
			soundText.gameObject.SetActive (true);
			musicOn.gameObject.SetActive (true);
			musicText.gameObject.SetActive (true);
			tutorial.gameObject.SetActive (true);


		}else if(n==4){
			quit.gameObject.SetActive (false);
			menu.gameObject.SetActive (false);
			madamCurie.gameObject.SetActive (false);

		}
	}

	public void GoToMainMenu(){
		SceneManager.LoadScene ("MainMenu");
	}

		
}
