﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CRBG : MonoBehaviour {
	private float timer;
	private bool sentinal;
	private float alpha;
	public Text text;

	GameManager gm;
	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager> ();
		alpha = 256f;
		sentinal = false;
		timer = 0;
		GetComponent<CanvasRenderer> ().SetAlpha (0);
		text.text = "";

	}
	
	// Update is called once per frame
	void Update () {
		if(timer<0 && sentinal){
			InvokeRepeating ("Deactivate", 0.001f, 0.05f);
			sentinal = false;
		}
	}
	public void Activate(int crNos){
		text.text = "X"+crNos.ToString ();
		CancelInvoke ("DecreaseTimer");
		GetComponent<CanvasRenderer> ().SetAlpha (alpha);
		sentinal = true;
		timer = 1f;
		InvokeRepeating ("DecreaseTimer", 0.001f, 1);
	}
	void Deactivate(){
		if (alpha > 0) {
			alpha -= 10;
			GetComponent<CanvasRenderer> ().SetAlpha (alpha);
		}else{
			alpha = 256;
			text.text = "";
			gm.AIPlay ();
			gm.ChangeTurn (2);
			CancelInvoke ("Deactivate");
		}

	}
	void DecreaseTimer(){
		if (timer >= 0) {
			timer -= 1f;
		}
	}
}
