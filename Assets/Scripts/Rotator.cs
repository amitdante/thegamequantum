﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
	public float rotateSpeed;
	public GameObject electronPoint;

	public bool moveToDual;
	public Vector3 dualPos;
	// Use this for initialization
	void Start () {
		moveToDual = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!moveToDual) {
			transform.RotateAround (transform.parent.position, Vector3.forward, rotateSpeed * Time.deltaTime);
		}else if(moveToDual){
			
			transform.position =  Vector3.MoveTowards (transform.position, dualPos, Time.deltaTime * 2);
		}
	}

	public void MoveToDual(Vector3 pos){
		dualPos = pos;
		GetComponent<CircleCollider2D> ().enabled = true;
		moveToDual = true;
	}
}
