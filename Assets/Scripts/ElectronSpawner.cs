﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ElectronSpawner : MonoBehaviour, IPointerClickHandler {
	public int type;
	public int owner;
	public int side;
	public GameObject electron1;
	public GameObject electron2;
	public List<GameObject> electrons;
	public bool readyFornext;
	public GameObject spawnPoint;
	public GameObject electronPoint;
	public GameObject vNucleus;

	int moveSpeed;
	private GameManager gm;
	public bool shake;
	private bool burst = true;
	// Use this for initialization
	void Awake(){
		shake = false;
	}
	void Start () {
		moveSpeed = 5;
		readyFornext = true;
		electrons = new List<GameObject>();
		gm = FindObjectOfType<GameManager> ();
		AddElectron ();

	}
	
	// Update is called once per frame
	void Update () {
		if(shake){
			foreach(GameObject electron in electrons){
				electron.GetComponent<Rotator> ().rotateSpeed *=4;
			}
			shake = false;
		}
	}

	#region IPointerClickHandler implementation


	public void OnPointerClick (PointerEventData eventData)
	{
		if(electrons.Count<4 && gm.turn==1 && owner==1){
			if(type == 3 && electrons.Count+1 < 4){
				gm.AIPlay ();
				gm.ChangeTurn (2);
			}else if(type == 2 && electrons.Count+1 < 3){
				gm.AIPlay ();
				gm.ChangeTurn (2);
			}else if(type == 1 && electrons.Count +1 <2){
				gm.AIPlay ();
				gm.ChangeTurn (2);
			}
			AddElectron ();


		}
	}


	#endregion



	void RepositionAll(int n){
		if(side ==1){
			for (int i = 0; i < n; i++) {
				electrons [i].transform.position = transform.GetChild (i+4).transform.position;
				electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
				electrons [i].transform.rotation = Quaternion.identity;
				electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
				transform.GetChild (i).gameObject.SetActive (false);
			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 2){
			for (int i = 0; i < n; i++) {
				if (i == 0 || i == 1) {
					electrons [i].transform.position = transform.GetChild (i + 3).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}else if(i==2){
					electrons [i].transform.position = transform.GetChild (i + 4).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}
			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 3){
			for (int i = 0; i < n; i++) {
				if (i == 0) {
					electrons [i].transform.position = transform.GetChild (i + 3).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}else if(i==2 || i == 1){
					electrons [i].transform.position = transform.GetChild (i + 4).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}
			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 4){
			for (int i = 0; i < n; i++) {
					electrons [i].transform.position = transform.GetChild (i + 3).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);

			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 5){
			for (int i = 0; i < n; i++) {
				electrons [i].transform.position = transform.GetChild (i + 3).transform.position;
				electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
				electrons [i].transform.rotation = Quaternion.identity;
				electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
				transform.GetChild (i).gameObject.SetActive (false);

			}
			StartCoroutine (Shoot (n , side));
		}
		else if(side == 6){
			for (int i = 0; i < n; i++) {
				electrons [i].transform.position = transform.GetChild (i + 4).transform.position;
				electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
				electrons [i].transform.rotation = Quaternion.identity;
				electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
				transform.GetChild (i).gameObject.SetActive (false);

			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 7){
			for (int i = 0; i < n; i++) {
				if (i == 0) {
					electrons [i].transform.position = transform.GetChild (i + 4).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}else if(i==1){
					electrons [i].transform.position = transform.GetChild (i + 5).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}

			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 8){
			for (int i = 0; i < n; i++) {
				if (i == 0) {
					electrons [i].transform.position = transform.GetChild (i + 3).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}else if(i==1){
					electrons [i].transform.position = transform.GetChild (i + 5).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}

			}
			StartCoroutine (Shoot (n , side));
		}else if(side == 9){
			for (int i = 0; i < n; i++) {
				if (i == 0) {
					electrons [i].transform.position = transform.GetChild (i + 3).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}else if(i==1){
					electrons [i].transform.position = transform.GetChild (i + 4).transform.position;
					electrons [i].GetComponent<Rotator> ().rotateSpeed = 0;
					electrons [i].transform.rotation = Quaternion.identity;
					electrons [i].GetComponent<CircleCollider2D> ().enabled = true;
					transform.GetChild (i).gameObject.SetActive (false);
				}

			}
			StartCoroutine (Shoot (n , side));
		}
	}
	public void AddElectron(){
		spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons++;
		if(owner==1){
			
			var electron = Instantiate (electron1);
			electron.transform.SetParent (this.transform);
			electrons.Add (electron);
			PositionElectrons (electron);
			gm.player1Score++;
			if(gm.crNos>0){
				gm.player1Score +=  gm.crNos;
				//spawnPoint.GetComponent<SpawnPointsScript> ().SetNucleusPoint (electrons.Count * gm.crNos);
			}

		}else if(owner==2){
			

			var electron = Instantiate (electron2);
			electron.transform.SetParent (this.transform);
			electrons.Add (electron);
			PositionElectrons (electron);
		}
	}

	void PositionElectrons(GameObject electron){
		if (type == 3) {
			if (electrons.Count == 1 && readyFornext) {
				readyFornext = false;
				electron.transform.position = new Vector3 (transform.position.x, transform.position.y + 0.40f, transform.position.z);
				SpawnPoint (electron.transform.position);
				readyFornext = true;

			} else if (electrons.Count == 2 && readyFornext) {
				readyFornext = false;
				electron.transform.position = Vector3.Normalize (electrons [electrons.IndexOf (electron) - 1].transform.position - transform.position) * -0.55f + transform.position;
				SpawnPoint (electron.transform.position);
				readyFornext = true;
			} else if (electrons.Count == 3 && readyFornext) {
				readyFornext = false;
				electron.transform.position = new Vector3 (transform.position.x, transform.position.y + 0.85f, transform.position.z);
				SpawnPoint (electron.transform.position);
				electron.GetComponent<Rotator> ().rotateSpeed *= -1;
				shake = true;
				readyFornext = true;
			} else if (electrons.Count == 4 && readyFornext) {
				readyFornext = false;
				electron.transform.position = Vector3.Normalize (electrons [electrons.IndexOf (electron) - 1].transform.position - transform.position) * -0.58f + transform.position;
				SpawnPoint (electron.transform.position);
				readyFornext = true;
				RepositionAll (4);
			}

		} else if (type == 2) {
			if (electrons.Count == 1 && readyFornext) {
				readyFornext = false;
				electron.transform.position = new Vector3 (transform.position.x, transform.position.y + 0.40f, transform.position.z);
				SpawnPoint (electron.transform.position);
				readyFornext = true;

			} else if (electrons.Count == 2 && readyFornext) {
				readyFornext = false;
				electron.transform.position = Vector3.Normalize (electrons [electrons.IndexOf (electron) - 1].transform.position - transform.position) * -0.55f + transform.position;
				SpawnPoint (electron.transform.position);
				readyFornext = true;
				shake = true;
			} else if (electrons.Count == 3 && readyFornext) {
				readyFornext = false;
				electron.transform.position = new Vector3 (transform.position.x, transform.position.y + 0.85f, transform.position.z);
				SpawnPoint (electron.transform.position);
				electron.GetComponent<Rotator> ().rotateSpeed *= -1;
				readyFornext = true;
				RepositionAll (3);
			}
		} else if (type == 1) {
			if (electrons.Count == 1 && readyFornext) {
				readyFornext = false;
				electron.transform.position = new Vector3 (transform.position.x, transform.position.y + 0.40f, transform.position.z);
				SpawnPoint (electron.transform.position);
				readyFornext = true;
				shake = true;
			} else if (electrons.Count == 2 && readyFornext) {
				readyFornext = false;
				electron.transform.position = Vector3.Normalize (electrons [electrons.IndexOf (electron) - 1].transform.position - transform.position) * -0.55f + transform.position;
				SpawnPoint (electron.transform.position);
				readyFornext = true;
				RepositionAll (2);
			}
		}

	}

	IEnumerator Shoot(int n,int side){
		//yield return new WaitForSeconds (0.2f);
		if(gm.turn==1){
			spawnPoint.GetComponent<SpriteRenderer> ().enabled = false;
			spawnPoint.transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = true;
			gm.ChainReaction (1,spawnPoint);
		}else{
			spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
			spawnPoint.transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = false;
			gm.ChainReaction (2,spawnPoint);
		}

//		if(owner==1){
//			if(gm.crAllowed){
//				gm.ChainReaction ();
//			}
//			if(!gm.crAllowed){
//				gm.crAllowed = true;
//				gm.ChainReaction ();
//			}
//		}
		if (n == 2 && side ==6) {
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.down * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 ) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}else if (n == 2 && side ==7) {
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.down * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.left * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 ) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}else if (n == 2 && side ==8) {
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.up * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.left * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 ) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}else if (n == 2 && side ==9) {
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.up * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 ) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}
		else if(n==3 && side ==1){
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			var electron3 = electrons [2];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.down * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
				if (electron3)
					electron3.transform.Translate (Vector2.left * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 && !electron3) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}else if(n==3 && side ==2){
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			var electron3 = electrons [2];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.up * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.down * Time.deltaTime * moveSpeed);
				if (electron3)
					electron3.transform.Translate (Vector2.left * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 && !electron3) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}else if(n==3 && side ==3){
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			var electron3 = electrons [2];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.up * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
				if (electron3)
					electron3.transform.Translate (Vector2.left * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 && !electron3) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}else if(n==3 && side ==4){
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			var electron3 = electrons [2];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.up * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.down * Time.deltaTime * moveSpeed);
				if (electron3)
					electron3.transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 && !electron3) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}
		else if(n==4 && side==5){
			var electron1 = electrons[0];
			var electron2 = electrons[1];
			var electron3 = electrons [2];
			var electron4 = electrons [3];
			electrons.Clear ();
			while (burst) {
				if (electron1)
					electron1.transform.Translate (Vector2.up * Time.deltaTime * moveSpeed);
				if (electron2)
					electron2.transform.Translate (Vector2.down * Time.deltaTime * moveSpeed);
				if (electron3)
					electron3.transform.Translate (Vector2.right * Time.deltaTime * moveSpeed);
				if (electron4)
					electron4.transform.Translate (Vector2.left * Time.deltaTime * moveSpeed);
				yield return new WaitForSeconds (0.01f);
				if (!electron1 && !electron2 && !electron3 && !electron4) {
					spawnPoint.GetComponent<CircleCollider2D> ().enabled = true;
					//spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
					//spawnPoint.GetComponent<SpawnPointsScript> ().RemoveNucleusPoint ();
					spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
					spawnPoint.GetComponent<SpawnPointsScript> ().m.isCritical = false;
					spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
					Destroy (gameObject);
					yield break;
				}
			}
		}
	}

	void SpawnPoint(Vector3 position){
		if (owner == 1) {
			var elecPoint = Instantiate (electronPoint);
			elecPoint.transform.position = position;
		}
	}


}
