﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VNucelus : MonoBehaviour {
	private int i;
	// Use this for initialization
	void Start () {
		i = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		i++;
		var _newPosition = transform.position;
		_newPosition.x += Mathf.Cos(70*i) * Time.deltaTime;
		transform.position = _newPosition ;
	}
}
