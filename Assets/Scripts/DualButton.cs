﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DualButton : MonoBehaviour {
	public GameObject dualMap;
	private GameManager gm;
	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OnClick(){
		if ((gm.turn == 1 && gm.p1Duals > 0) ) {
			dualMap.SetActive (true);
		}
	}
}
