﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {
	public GameObject otherNucleus;
	private int electronCount;
	private ElectronSpawner es;
	private GameManager gm;
	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager> ();
	}

	// Update is called once per frame
	void Update () {

	}
	void OnTriggerEnter2D(Collider2D other){
		electronCount = GetComponent<ElectronSpawner> ().electrons.Count;
		if ((GetComponent<ElectronSpawner> ().owner == 1 && other.gameObject.tag == "electron2") || (GetComponent<ElectronSpawner> ().owner == 2 && other.gameObject.tag == "electron1")) {
			other.gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m.electrons = 0;
			GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().spawned = false;
			var nucleus = Instantiate (otherNucleus);
			nucleus.transform.position = transform.position;
			nucleus.GetComponent<ElectronSpawner> ().side = GetComponent<ElectronSpawner> ().side;
			nucleus.GetComponent<ElectronSpawner> ().type = GetComponent<ElectronSpawner> ().type;
			nucleus.GetComponent<ElectronSpawner> ().spawnPoint = GetComponent<ElectronSpawner> ().spawnPoint;
			nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m = GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m;
			nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().spawned = true;
			if(other.gameObject.tag=="electron1"){
				gm.player1Score--;
				nucleus.GetComponent<ElectronSpawner>().spawnPoint.GetComponent<SpriteRenderer> ().enabled = true;
				nucleus.GetComponent<ElectronSpawner>().spawnPoint.transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = false;
				nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 1;
				GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 1;

			}else if(other.gameObject.tag=="electron2" && GetComponent<ElectronSpawner> ().owner == 1){
				nucleus.GetComponent<ElectronSpawner>().spawnPoint.GetComponent<SpriteRenderer> ().enabled = false;
				nucleus.GetComponent<ElectronSpawner>().spawnPoint.transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = true;
				gm.player1Score -= GetComponent<ElectronSpawner> ().electrons.Count;
				nucleus.GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 2;
				GetComponent<ElectronSpawner> ().spawnPoint.GetComponent<SpawnPointsScript> ().m.owner = 2;
				nucleus.transform.SetParent (nucleus.GetComponent<ElectronSpawner> ().spawnPoint.gameObject.transform);
			}
			gm.HandleCollision (nucleus, electronCount);
			Destroy (other.gameObject);
			Destroy (gameObject);

		} else if ((GetComponent<ElectronSpawner> ().owner == 1 && other.gameObject.tag == "electron1") || (GetComponent<ElectronSpawner> ().owner == 2 && other.gameObject.tag == "electron2")) {
			other.gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			if(other.gameObject.tag=="electron1"){
				gm.player1Score--;
			}
			Destroy (other.gameObject);
			GetComponent<ElectronSpawner> ().AddElectron ();

		}
	}
		
}
