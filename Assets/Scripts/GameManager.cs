﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class GameManager : MonoBehaviour {
	public SpawnPointsScript[] spawnPoints;
	public Text scoretext;
	public int turn;
	public int player1Score;
	public GameObject p1Slider;
	public GameObject p2Slider;
	public bool crAllowed;
	public int crNos;
	public Image CRBG;
	public int p1Duals;
	public int p2Duals;
	public Text p1DualText;
	public Text p2DualText;
	public BoardAI ai;
	public MadamCurieLogo curie;
	public ScoreBase ScoreBase;
	public AchBase AchBase;
	public PanelSlider panel;
	public Text achText;
	public GameObject placeHolder;
	public GameObject spawnMap;
	[HideInInspector]
	public GameObject lastReactionPoint;
	[HideInInspector]
	public List<GameObject> chainReactionsBuffer;
	[HideInInspector]
	public int uniqueReactions;


	float aiReturnScore;
	private string scoreString;
	private int aiLevel;


	// Use this for initialization
	void Start () {
		crNos = 0;
		turn = 1;
		crAllowed = false;
		p1Duals = 5;
		p2Duals = 5;
		spawnPoints = FindObjectsOfType<SpawnPointsScript> ();
		chainReactionsBuffer = new List<GameObject> ();
		aiLevel = GetComponent<SaveData> ().difficultyLevel;
		achText.text = (GetComponent<SaveData> ().totalAchievements) + "/28";
		InvokeRepeating ("CheckWin", 0.5f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
//		if(Input.GetKeyDown(KeyCode.Space)){
//			foreach(Move m in ai.board.allMoves){
//				Debug.Log (m.point.gameObject.name + " : " + m.owner);
//			}
//		}
//		if(Input.GetKeyDown(KeyCode.A)){
//			SpawnPointsScript[] point = FindObjectsOfType<SpawnPointsScript> ();
//			Debug.Log (point.Length);
//			foreach(SpawnPointsScript m in point){
//				Debug.Log (m.m.point.gameObject.name + " : " + m.m.owner);
//			}
//		}
//		if(turn == 1){
//			p1Slider.SetActive (true);
//			p2Slider.SetActive (false);
//		}else if(turn ==2){
//			p2Slider.SetActive (true);
//			p1Slider.SetActive (false);
//		}
		UpdateScore (player1Score);
	}

	public void HandleCollision(GameObject nucleus, int electronCount){
		StartCoroutine (CollisionHandler (nucleus, electronCount));
	}

	IEnumerator CollisionHandler(GameObject nucleus, int electronCount){
		int i=0;
		while(i<electronCount){
			yield return new WaitForSeconds (0.1f);
			nucleus.GetComponent<ElectronSpawner> ().AddElectron ();
			i++;
		}
		yield break;
	}

	void UpdateScore(int score){
		scoreString = score.ToString ();
		if(scoreString.Length == 1){
			scoretext.text = "000" + scoreString;
		}else if(scoreString.Length == 2){
			scoretext.text = "00" + scoreString;
		}else if(scoreString.Length == 3){
			scoretext.text = "0" + scoreString;
		}else{
			scoretext.text = scoreString;
		}

	}

	public void ChainReaction(int p, GameObject point){
		
		if (p == 1) {
			if (!chainReactionsBuffer.Contains(point)){
				chainReactionsBuffer.Add (point);
				uniqueReactions++;
			}
			lastReactionPoint = point;
			crNos++;
			GetComponent<Timer> ().ResetTimer ();
		}
		if (p == 2) {
			GetComponent<Timer> ().ResetTimer2 ();
		}

	}

	public void DualUsed(){
		p1DualText.text = p1Duals.ToString ();
		p2DualText.text = p2Duals.ToString ();

	}

	public void AIPlay(){
		
		StartCoroutine ("PlayAI");
	}

	IEnumerator PlayAI(){
		
		yield return new WaitForSeconds (0.5f);
		//float score = 99999.2356f;

		aiReturnScore = 99999.234f;
		Thread aiThread = new Thread (new ThreadStart(CallNegaMax ));
		aiThread.Start ();
		//score = ai.ABNegamax (ai.board, 1, 5, 1,ref ai.move,Mathf.NegativeInfinity,Mathf.Infinity);
		//score = ai.Negamax (ai.board, 1, 5, 1,ref ai.move);
		//float score = ai.Minimax (ai.board, 1, 3, 1,ref ai.move);
		yield return new WaitUntil (() => aiReturnScore != 99999.234f);
		//yield return new WaitUntil (() => score != 99999.2356f);
		//aiThread.Join ();
		//Debug.Log (ai.move.point.gameObject.name);
		aiThread.Abort ();
		Debug.Log (ai.move.point.gameObject.name + " returned with score : " + aiReturnScore);
		if (ai.move.owner == 0) {
			ai.move.point.NucleusSpawnerAI ();
			ChangeTurn (1);
		}else if(ai.move.owner==2){
			if(ai.move.electrons<ai.move.criticalMass-1){
				ChangeTurn (1);
			}
			ai.move.point.gameObject.transform.GetChild (1).GetComponent<ElectronSpawner> ().AddElectron ();
		}
		crNos = 0;
		ai.move = null;
		ai.board.firstTurn = false;
		StopCoroutine ("PlayAI");

	}

	void CallNegaMax(){
		aiReturnScore = ai.ABNegamax(ai.board, 1,aiLevel, 1,ref ai.move,Mathf.NegativeInfinity,Mathf.Infinity);
	}

	public void ChangeTurn(int p){
		if(p==2){
			curie.MoveDown ();
			AchBase.MoveDown ();
			ScoreBase.MoveDown ();
			turn = 2;
			foreach(SpawnPointsScript point in spawnPoints){
				if (point.m.owner==0) {
					point.gameObject.GetComponent<SpriteRenderer> ().enabled = false;
					point.gameObject.transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = true;
				}
			}
		}else{
			chainReactionsBuffer.Clear ();
			uniqueReactions = 0;
			curie.MoveUp ();
			AchBase.MoveUp ();
			ScoreBase.MoveUp ();
			turn = 1;
			foreach(SpawnPointsScript point in spawnPoints){
				if (point.m.owner==0) {
					point.gameObject.GetComponent<SpriteRenderer> ().enabled = true;
					point.gameObject.transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = false;
				}
			}
		}
	}

	public void CheckWin(){
		int aiSpots = 0;
		int playerSpots = 0;
		foreach(Move m in ai.board.allMoves){
			if(m.owner == 1){
				playerSpots++;
			}else if(m.owner == 2){
				aiSpots++;
			}
		}
		if((aiSpots == 0 && playerSpots>0 &&!ai.board.firstTurn)){
			if(GetComponent<SaveData> ().lastResult==1){
				GetComponent<SaveData> ().contiWins++;
			}else if(GetComponent<SaveData> ().lastResult==-1){
				GetComponent<SaveData> ().contiWins = 1;
			}
			GetComponent<SaveData> ().contiLoss = 0;
			GetComponent<SaveData> ().lastResult = 1;
			GetComponent<SaveData> ().wins++;
			panel.WinScreen ();
			GetComponent<SaveData> ().Save ();
			Debug.Log ("RESULT DECLARED PLAYER WINS !!!");
			CancelInvoke ("CheckWin");
			spawnMap.SetActive (false);
		}else if((playerSpots==0 && aiSpots>0)){
			
			if(GetComponent<SaveData> ().lastResult==1){
				GetComponent<SaveData> ().contiLoss = 1;
			}else if(GetComponent<SaveData> ().lastResult==-1){
				GetComponent<SaveData> ().contiLoss++;
			}
			GetComponent<SaveData> ().contiWins = 0;
			GetComponent<SaveData> ().lastResult = -1;
			GetComponent<SaveData> ().losses++;
			panel.LooseScreen ();
			GetComponent<SaveData> ().Save ();
			Debug.Log ("RESULT DECLARED AI WINS!");
			CancelInvoke ("CheckWin");
			spawnMap.SetActive (false);
		}

	}

	public void UpdateAchievement(){
		//achText.text = uniqueReactions.ToString() + "/25"; 
		if(uniqueReactions>0){
			UnlockCharacter (uniqueReactions);
		}

	}

	void UnlockCharacter(int n){
		
		if(!GetComponent<SaveData>().uniqueReactionsUnlocked.Contains(n)){
			GetComponent<SaveData> ().uniqueReactionsUnlocked.Add (n);
			placeHolder.SetActive (true);
			placeHolder.transform.GetChild (0).GetComponent<Text> ().text = "Unlocked " + n;
			Invoke ("RemovePlaceHolder", 0.5f);
			GetComponent<SaveData> ().totalAchievements++;
			GetComponent<SaveData> ().Save ();
			achText.text = (GetComponent<SaveData> ().totalAchievements) + "/28";
			switch(n){
			case 1:
				Debug.Log ("Character 1 unlocked");
				break;
			case 2:
				Debug.Log ("character 2 unlocked");
				break;
			case 3:
				Debug.Log ("Character 3 unlockde");
				break;
			}
		}
	}

	void RemovePlaceHolder(){
		placeHolder.SetActive (false);
	}
}
