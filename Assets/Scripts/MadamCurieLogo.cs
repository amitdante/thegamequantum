﻿using UnityEngine;

public class MadamCurieLogo : MonoBehaviour {

	public RectTransform canvas;
	RectTransform button;
	public RectTransform Limit;
	Vector3 startingPosition;
	public float speed;
	bool canMoveUp;
	bool canMoveDown;

	void Start()
	{
		button = gameObject.GetComponent<RectTransform>();
		//canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
		startingPosition = transform.position;
		speed = 0.25f;
		canMoveUp = true;
		canMoveDown = false;

	}

	void Update () {
		if (canMoveUp) {
			transform.Translate (0f, speed, 0f);
			if (button.position.y > Limit.position.y) {
				canMoveUp = false;
			}
		} else if (canMoveDown) {
			transform.Translate (0f, -speed, 0f);
			if (button.position.y <= startingPosition.y) {
				canMoveDown = false;
			}
		}
	}
	public void MoveUp(){
		canMoveDown = false;
		canMoveUp = true;
	}
	public void MoveDown(){
		canMoveDown = true;
		canMoveUp = false;
	}
}