﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour {
	private Rigidbody2D rb;

	void Start(){
		rb = GetComponent<Rigidbody2D> ();

	}
	void Update(){
		rb.velocity = new Vector3 (Random.Range (-5, 5), Random.Range (-5, 5), 0);
		var pos = Camera.main.WorldToScreenPoint (transform.position);
		if(pos.x<0 || pos.x>Screen.width){
			rb.velocity *= -1;
		}
	}
}
