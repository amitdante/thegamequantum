﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronPoint : MonoBehaviour {
	private SpriteRenderer sr;
	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		Invoke ("DestroySelf", 1f);
		InvokeRepeating ("DecreaseAlpha", 0.5f, 0.15f);
	}

	void Update(){
		transform.position = new Vector3 (transform.position.x, transform.position.y + Time.deltaTime*2f, transform.position.z);
	}
	void DestroySelf(){
		Destroy (gameObject);
	}

	void DecreaseAlpha(){
		var color = sr.color;
		color.a -= 0.8f;
		sr.color = color;
	}
}
