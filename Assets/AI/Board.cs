﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board
{
	public List<Move> allMoves;

	public bool firstTurn = true;
    public Board()
    {
		allMoves = new List<Move> ();
    }
    

	public virtual List<Move> GetMoves(int player)
    {
		List<Move> moves = new List<Move> ();
		foreach(Move m in allMoves){
			if(player==1){
				if(m.owner==0 || (m.owner==2)){
					moves.Add (m);
				}
			}else{
				if(m.owner==0 || (m.owner==1 )){
					moves.Add (m);
				}
			}
		}
		return moves;
    }

    public virtual Board MakeMove(Move m,int player)
    {
		Board newBoard = new Board ();
		newBoard.firstTurn = false;
		newBoard.allMoves.Clear ();
		foreach(Move move in allMoves){
			Move newMove = new Move (move.point, move.owner, move.id, move.x, move.y,move.criticalMass, move.electrons, move.isCritical);
			newBoard.allMoves.Add (newMove);
		}

		foreach (Move move in newBoard.allMoves) {
			if (move.id == m.id) {
				
				if (player == 1) {
					move.owner = 2;
					move.electrons++;

					if ((move.criticalMass - move.electrons) == 1) {
						move.isCritical = true;
					}

					if (move.electrons == move.criticalMass) {
						
						ChainReaction (move, newBoard , 1);

					}

				} else {
					move.owner = 1;
					move.electrons++;

					if ((move.criticalMass - move.electrons) == 1) {
						move.isCritical = true;

					}

					if (move.electrons == move.criticalMass) {
						
						ChainReaction (move, newBoard, 1);

					}
				

				}
			}
		}

		return newBoard;
    }

	public virtual bool IsGameOver()
    {
		
		int aiSpots = 0;
		int playerSpots = 0;
		foreach(Move m in allMoves){
			if(m.owner == 1){
				playerSpots++;
			}else if(m.owner == 2){
				aiSpots++;
			}
		}
		if((aiSpots == 0 && playerSpots>0 &&!firstTurn) || (playerSpots==0 && aiSpots>0)){
			return true;
		}
		return false;
    }
		

	public float OldEvaluate(int player){
		#region Win AND Loose
		int aiSpots = 0;
		int playerSpots = 0;
		foreach(Move m in allMoves){
			if(m.owner == 1){
				playerSpots++;
			}else if(m.owner == 2){
				aiSpots++;
			}
		}
		if(aiSpots == 0 && playerSpots>0 && !firstTurn && player==1){
			
		
			return -10000;
		}else if(playerSpots == 0 && aiSpots>0 && player==1){
			
			return 10000;
		}
		if(aiSpots == 0 && playerSpots>0 && !firstTurn && player==-1){

			return 10000;
		}else if(playerSpots == 0 && aiSpots>0 && player==-1){

			return -10000;
		}
		#endregion
		float returnValue = 0;
		#region For AI Player

		if (player == 1) {
			
			foreach (Move m in allMoves) {
				bool criticalFound = false;

				if (m.owner == 2) {
					List<Move> neighbors = GetNeighbors (m);
					foreach (Move mv in neighbors) {
						if (mv.owner == 1 && mv.electrons == mv.criticalMass-1) {
							returnValue = (returnValue - ((5 - mv.criticalMass)) * m.electrons);
							criticalFound = true;
						}

					}
					if (!criticalFound) {
						if (m.criticalMass == 2) {
							returnValue += 3 * m.electrons;
						} else if (m.criticalMass == 3) {
							returnValue += 2 * m.electrons;
						}
						if (m.isCritical) {
							returnValue += 2;
						}
					}
					if (m.owner == 2) {
						returnValue += m.electrons;
					}
//					if (m.electrons == m.criticalMass) {
//						returnValue += 10 * m.electrons;
//					}
				} else if (m.owner == 1) {
					List<Move> neighbors = GetNeighbors (m);
					foreach (Move mv in neighbors) {
						if (mv.owner == 2 && mv.electrons == mv.criticalMass-1) {
							returnValue = (returnValue + (5 - mv.criticalMass)) * m.electrons;
							criticalFound = true;
						}

					}
					if (!criticalFound) {
						if (m.criticalMass == 2) {
							returnValue -= 3 * m.electrons;
						} else if (m.criticalMass == 3) {
							returnValue -= 5 * m.electrons;
						}
						if (m.isCritical) {
							returnValue -= 2;
						}

					}

				}

			}
			int critFound = 0;
			bool firstFound = false;
			int prevX = 0;
			int prevY = 0;
			for (int x = 1; x <= 4; x++) {
				for (int y = 1; y <= 7; y++) {
					foreach (Move m in allMoves) {
						if (m.owner == 2) {
							if (m.x == x && m.y == y && m.isCritical && !firstFound) {
								critFound++;
								firstFound = true;
								prevX = x;
								prevY = y;
							} else if (m.x == x && m.y == y && m.isCritical && firstFound) {
								if (m.x == prevX && m.y == prevY + 1) {
									critFound++;
									prevX = x;
									prevY = y;
								} else {
									critFound = 1;
								}
							}
						}
					}

				}
				returnValue += 2 * critFound;
				critFound = 0;
				firstFound = false;
				prevX = 0;
				prevY = 0;
			}
			for (int y = 1; y <= 7; y++) {
				for (int x = 1; x <= 4; x++) {
					foreach (Move m in allMoves) {
						if (m.owner == 2) {
							if (m.x == x && m.y == y && m.isCritical && m.owner == 2 && !firstFound) {
								critFound++;
								firstFound = true;
								prevX = x;
								prevY = y;
							} else if (m.x == x && m.y == y && m.isCritical && m.owner == 2 && firstFound) {
								if (m.x == prevX + 1 && m.y == prevY) {
									critFound++;
									prevX = x;
									prevY = y;
								} else {
									critFound = 1;
								}				
							}
						}

					}
				}
				returnValue += 2 * critFound;
				critFound = 0;
				firstFound = false;
				prevY = 0;
				prevX = 0;
			}
		}
		#endregion
		#region For Human Player
		if (player == -1) {

			foreach (Move m in allMoves) {
				bool criticalFound = false;

				if (m.owner == 1) {
					List<Move> neighbors = GetNeighbors (m);
					foreach (Move mv in neighbors) {
						if (mv.owner == 2 && mv.electrons==mv.criticalMass-1) {
							returnValue = (returnValue - ((5 - mv.criticalMass)) * m.electrons);
							criticalFound = true;
						}

					}
					if (!criticalFound) {
						if (m.criticalMass == 2) {
							returnValue += 3 * m.electrons;
						} else if (m.criticalMass == 3) {
							returnValue += 2 * m.electrons;
						}
						if (m.isCritical) {
							returnValue += 2;
						}
					}
					if (m.owner == 1) {
						returnValue += m.electrons;
					}
//					if (m.electrons == m.criticalMass) {
//						returnValue += 10 * m.electrons;
//					}
				} else if (m.owner == 2) {
					List<Move> neighbors = GetNeighbors (m);
					foreach (Move mv in neighbors) {
						if (mv.owner == 1 && mv.electrons==mv.criticalMass-1) {
							returnValue = (returnValue + (5 - mv.criticalMass)) * m.electrons;
							criticalFound = true;
						}

					}
					if (!criticalFound) {
						if (m.criticalMass == 2) {
							returnValue -= 3 * m.electrons;
						} else if (m.criticalMass == 3) {
							returnValue -= 5 * m.electrons;
						}
						if (m.isCritical) {
							returnValue -= 2;
						}

					}

				}

			}
			int critFound = 0;
			bool firstFound = false;
			int prevX = 0;
			int prevY = 0;
			for (int x = 1; x <= 4; x++) {
				for (int y = 1; y <= 7; y++) {
					foreach (Move m in allMoves) {
						if (m.owner == 1) {
							if (m.x == x && m.y == y && m.isCritical && !firstFound) {
								critFound++;
								firstFound = true;
								prevX = x;
								prevY = y;
							} else if (m.x == x && m.y == y && m.isCritical && firstFound) {
								if (m.x == prevX && m.y == prevY + 1) {
									critFound++;
									prevX = x;
									prevY = y;
								} else {
									critFound = 1;
								}
							}
						}
					}

				}
				returnValue += 2 * critFound;
				critFound = 0;
				firstFound = false;
				prevX = 0;
				prevY = 0;
			}
			for (int y = 1; y <= 7; y++) {
				for (int x = 1; x <= 4; x++) {
					foreach (Move m in allMoves) {
						if (m.owner == 1) {
							if (m.x == x && m.y == y && m.isCritical && m.owner == 2 && !firstFound) {
								critFound++;
								firstFound = true;
								prevX = x;
								prevY = y;
							} else if (m.x == x && m.y == y && m.isCritical && m.owner == 2 && firstFound) {
								if (m.x == prevX + 1 && m.y == prevY) {
									critFound++;
									prevX = x;
									prevY = y;
								} else {
									critFound = 1;
								}				
							}
						}

					}
				}
				returnValue += 2 * critFound;
				critFound = 0;
				firstFound = false;
				prevY = 0;
				prevX = 0;
			}
		}
		#endregion
		
		return returnValue;
    }

    // new function for negamax
	public float Evaluate(int player)
    {
		#region Win AND Loose
		int aiSpots = 0;
		int playerSpots = 0;
		foreach(Move m in allMoves){
			if(m.owner == 1){
				playerSpots++;
			}else if(m.owner == 2){
				aiSpots++;
			}
		}
		if(aiSpots == 0 && playerSpots>0 && !firstTurn && player==1){
			

			return -10000;
		}else if(playerSpots == 0 && aiSpots>0 && player==1){
			
			return 10000;
		}
		if(aiSpots == 0 && playerSpots>0 && !firstTurn && player==-1){
			
			return 10000;
		}else if(playerSpots == 0 && aiSpots>0 && player==-1){
			
			return -10000;
		}
		#endregion

		float returnValue = 0;

		//PLAYER AI
		if(player==1){
			foreach(Move m in allMoves){
				if(m.owner==2 && m.criticalMass==2){
					returnValue += 3;
				}else if(m.owner==2 && m.criticalMass==3){
					returnValue += 2;
				}else if(m.owner==2 && m.criticalMass==4){
					returnValue++;
				}else if(m.owner==1 && m.criticalMass==2){
					returnValue -= 3;
				}else if(m.owner==1 && m.criticalMass==3){
					returnValue -= 2;
				}else if(m.owner==1 && m.criticalMass==4){
					returnValue--;
				}
			}
		}
		//PLAYER HUMAN
		if(player==-1){
			foreach(Move m in allMoves){
				if(m.owner==2 && m.criticalMass==2){
					returnValue -= 3;
				}else if(m.owner==2 && m.criticalMass==3){
					returnValue -= 2;
				}else if(m.owner==2 && m.criticalMass==4){
					returnValue--;
				}else if(m.owner==1 && m.criticalMass==2){
					returnValue += 3;
				}else if(m.owner==1 && m.criticalMass==3){
					returnValue += 2;
				}else if(m.owner==1 && m.criticalMass==4){
					returnValue++;
				}
			}
		}

		return returnValue;
	}

	List<Move> GetNeighbors(Move m){
		List<Move> neighbors = new List<Move> ();
		foreach(Move move in allMoves){
			if((move.x==m.x && move.y==m.y+1) || (move.x==m.x && move.y==m.y-1) || (move.x==m.x+1 && move.y==m.y) || (move.x==m.x-1 && move.y==m.y)){
				neighbors.Add (move);
			}
		}
		return neighbors;
	}

	List<Move> GetNewNeighbors(Move m,Board b){
		List<Move> neighbors = new List<Move> ();
		foreach(Move move in b.allMoves){
			if((move.x==m.x && move.y==m.y+1) || (move.x==m.x && move.y==m.y-1) || (move.x==m.x+1 && move.y==m.y) || (move.x==m.x-1 && move.y==m.y)){
				neighbors.Add (move);
			}
		}
		return neighbors;
	}

	void ChainReaction(Move m, Board b, int depth){
		if(depth==30){
			return;
		}
		depth++;
		int owner = m.owner;
		m.owner = 0;
		m.electrons = 0;
		m.isCritical = false;
		List<Move> neighbors = GetNewNeighbors (m,b);
		foreach(Move neighbor in neighbors){
				neighbor.electrons++;
				neighbor.owner = owner;
				if(neighbor.electrons==neighbor.criticalMass){
					ChainReaction(neighbor, b, depth);
				}
		}
	}

}
