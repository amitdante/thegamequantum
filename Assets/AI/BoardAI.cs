﻿using UnityEngine;
using System.Collections;

public class BoardAI : MonoBehaviour
{	
	public Board board;
	public Move move;

	void Start(){
		board = new Board ();
	}

	void Update(){
	//if (Input.GetKeyDown (KeyCode.Space)) {

//			//float score = ABNegamax (board, 1, 5, 1,ref move,Mathf.NegativeInfinity,Mathf.Infinity);
////			if(move!=null)
////			Debug.Log ("Score : " + score + " and move : " + move.point.gameObject.name);
//
		//}
	}

//	public float Minimax(Board board,int player,int maxDepth,int currentDepth,ref Move bestMove){
//		if (board.IsGameOver () || currentDepth == maxDepth) {
//			return board.Evaluate ();
//		}
//
//		bestMove = null;
//        float bestScore = Mathf.Infinity;
//		if (player == 1) {
//			bestScore = Mathf.NegativeInfinity;
//		}
//
//        foreach (Move m in board.GetMoves(player))
//        {
//			Board b = board.MakeMove(m,player);
//			float currentScore;
//			Move currMove = null;
//			currentScore = Minimax(b, -player, maxDepth, currentDepth + 1,ref currMove);
//
//			if (player==1)
//            {
//				if (currentScore> bestScore)
//                {
//					bestScore = currentScore;
//					bestMove= m;
//                }
//            }
//            else
//            {
//				if (currentScore < bestScore)
//                {
//					bestScore = currentScore;
//					bestMove = m;
//                }
//            }
//        }
//
//		return bestScore;
//    }



//
//    public float Negamax(
//            Board board,
//			int player,
//            int maxDepth,
//            int currentDepth,
//            ref Move bestMove)
//    {
//        if (board.IsGameOver() || currentDepth == maxDepth)
//            return board.Evaluate(player);
//
//        bestMove = null;
//        float bestScore = Mathf.NegativeInfinity;
//
//        foreach (Move m in board.GetMoves(player))
//        {
//            Board b = board.MakeMove(m,player);
//            float recursedScore;
//            Move currentMove = null;
//			recursedScore = Negamax(b,-player, maxDepth, currentDepth + 1, ref currentMove);
//            float currentScore = -recursedScore;
//
//            if (currentScore > bestScore)
//            {
//                bestScore = currentScore;
//				bestMove = m;
//            }
//        }
//        
//        return bestScore;
//    }

	public float ABNegamax(
            Board board,
            int player,
            int maxDepth,
            int currentDepth,
		ref Move bestMove,
            float alpha,
            float beta)
    {
		if (board.IsGameOver () || currentDepth == maxDepth) {
				return board.Evaluate (player);
		}

        bestMove = null;
        float bestScore = Mathf.NegativeInfinity;
        foreach (Move m in board.GetMoves(player))
		{	
			
			Board b = board.MakeMove(m,player);
            float recursedScore;
            Move currentMove = null;
            int cd = currentDepth + 1;
            float max = Mathf.Max(alpha, bestScore);
            recursedScore = ABNegamax(b, -player, maxDepth, cd, ref currentMove, -beta,-max);		
            float currentScore = -recursedScore;
			currentScore -= currentDepth;

            if (currentScore > bestScore)
            {
                bestScore = currentScore;
                bestMove = m;

                if (bestScore >= beta)
                    return bestScore;
            }

        }

        return bestScore;
    }

}


