﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//public class ReturnData {
//
//	public float Evaluate(int player){
//		#region WIN CONDITION OR LOSE !
//		int aiSpots = 0;
//		int playerSpots = 0;
//		foreach(Move m in allMoves){
//			if(m.owner == 1){
//				playerSpots++;
//			}else if(m.owner == 2){
//				aiSpots++;
//			}
//		}
//		if(aiSpots == 0 && playerSpots>0 && !firstTurn && player==1){
//			return -10000;
//		}else if(playerSpots == 0 && aiSpots>0 && player==1){
//			Debug.Log ("AI WINS !!! ");
//			return 10000;
//		}
//		if(aiSpots == 0 && playerSpots>0 && !firstTurn && player==-1){
//			return 10000;
//		}else if(playerSpots == 0 && aiSpots>0 && player==-1){
//			return -10000;
//		}
//		#endregion
//		if (player == 1) {
//			float returnValue = 0;
//			foreach (Move m in allMoves) {
//				bool criticalFound = false;
//				List<Move> neighbors = GetNeighbors (m);
//				if (m.owner == 2) {
//					foreach (Move mv in neighbors) {
//						if (mv.owner == 1 && mv.isCritical) {
//							returnValue = (returnValue - ((5 - mv.criticalMass)) * m.electrons);
//							criticalFound = true;
//						}
//
//					}
//					if (!criticalFound) {
//						if (m.criticalMass == 2) {
//							returnValue += 3 * m.electrons;
//						} else if (m.criticalMass == 3) {
//							returnValue += 2 * m.electrons;
//						}
//						if (m.isCritical) {
//							returnValue += 2;
//						}
//					}
//					if (m.owner == 2) {
//						returnValue += m.electrons;
//					}
//					if (m.electrons == m.criticalMass) {
//						returnValue += 10 * m.electrons;
//					}
//				}
//			}
//		}
//		else if(m.owner==1){
//			foreach (Move mv in neighbors) {
//				if (mv.owner == 2 && mv.isCritical) {
//					returnValue = (returnValue + (5 - mv.criticalMass))*m.electrons;
//					criticalFound = true;
//				}
//
//			}
//			if (!criticalFound ) {
//				if (m.criticalMass == 2) {
//					returnValue -= 3*m.electrons;
//				} else if (m.criticalMass == 3) {
//					returnValue -= 5*m.electrons;
//				}
//				if (m.isCritical) {
//					returnValue -= 2;
//				}
//
//			}
//			if(m.electrons==m.criticalMass){
//				returnValue -= 5 * m.electrons;
//			}
//			if (m.owner == 1) {
//				returnValue -= m.electrons;
//			}
//		}
//
//	}
//	int critFound = 0;
//	bool firstFound = false;
//	int prevX = 0;
//	int prevY = 0;
//	for (int x = 1; x <= 4; x++) {
//		for(int y= 1; y<=7;y++){
//			foreach(Move m in allMoves){
//				if (m.owner == 2) {
//					if (m.x == x && m.y == y && m.isCritical && !firstFound) {
//						critFound++;
//						firstFound = true;
//						prevX = x;
//						prevY = y;
//					} else if (m.x == x && m.y == y && m.isCritical && firstFound) {
//						if (m.x == prevX && m.y == prevY + 1) {
//							critFound++;
//							prevX = x;
//							prevY = y;
//						} else {
//							critFound = 1;
//						}
//					}
//				}
//			}
//
//		}
//		returnValue += 2 * critFound;
//		critFound = 0;
//		firstFound = false;
//		prevX = 0;
//		prevY = 0;
//	}
//	for (int y = 1; y <= 7; y++) {
//		for (int x = 1; x <= 4; x++) {
//			foreach (Move m in allMoves) {
//				if (m.owner == 2) {
//					if (m.x == x && m.y == y && m.isCritical && m.owner == 2 && !firstFound) {
//						critFound++;
//						firstFound = true;
//						prevX = x;
//						prevY = y;
//					} else if (m.x == x && m.y == y && m.isCritical && m.owner == 2 && firstFound) {
//						if (m.x == prevX + 1 && m.y == prevY) {
//							critFound++;
//							prevX = x;
//							prevY = y;
//						} else {
//							critFound = 1;
//						}				
//					}
//				}
//
//			}
//		}
//		returnValue += 2 * critFound;
//		critFound = 0;
//		firstFound = false;
//		prevY = 0;
//		prevX = 0;
//	}
//	return returnValue;
//}
