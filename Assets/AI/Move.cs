﻿using UnityEngine;
using System.Collections;

public class Move
{
	public SpawnPointsScript point;
	public int owner;
	public float id;
	public int x;
	public int y;
	public int electrons;
	public int criticalMass;
	public bool isCritical;

	public Move(SpawnPointsScript point,int owner,float id, int x, int y, int criticalMass,int electrons, bool isCritical){;
		this.point = point;
		this.owner = owner;
		this.id = id;
		this.x = x;
		this.y = y;
		this.electrons = electrons;
		this.criticalMass = criticalMass;
		this.isCritical = isCritical;
		
	}

	public Move(){
		
	}

}
